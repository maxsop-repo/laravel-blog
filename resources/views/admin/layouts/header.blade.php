<!-- body nav start -->
<nav class="main-nav">
    <ul class="float-left">
        <li>
            <a href="#" id="aside-toggle"><i class="ion-android-menu io-25"></i></a>
        </li>
    </ul>

    <ul class="float-right">
        <!-- notifications -->
        <li class="dropdown">
            <a href="#" class="menu-button">
                <i class="ion-android-notifications-none io-24"></i>
            </a>
            <ul class="sub-menu">
                <li class="head">
                    <a href="#">
                        <h6>You have 2 notifications</h6>
                    </a>
                </li>
                <li><a href="#">Settings</a></li>
                <li>
                    <a href="#">
                        <span>Awesome aminmate.css</span>
                        <small>10 minit ago</small>
                    </a>
                </li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Logout</a></li>
            </ul>
        </li>

        <!-- settings -->
        <li>
            <a href="#" class="settings">
                <i class="ion-android-settings io-24"></i>
            </a>
        </li>

        <!-- profile -->
        <li class="dropdown">
            <a href="#" class="menu-button">
                <img src="{{ asset('admin/images/avatars/male-md.png') }}" alt="">
            </a>

            <ul class="sub-menu">
                <li class="head">
                    <a href="#">
                        <h6>Username</h6>
                        <small>example@gmail.com</small>
                    </a>
                </li>

                <li>
                    <a href="#">Settings</a>
                </li>

                <li>
                    <a href="#">
                        <span>Profile</span>
                        <span class="badge float-right">30%</span>
                    </a>
                </li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Logout</a></li>
            </ul>
        </li>
    </ul>
</nav>
<!-- body nav end -->
