<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {
    
    public function posts() {
    	return $this->belongsToMany('App\Model\User\Post', 'category_posts', 'category_id', 'post_id');
    }
}
