@extends('admin.layouts.app')

@section('main-content')
<!-- body container start -->
<div class="body-container">
    <div class="container-fluid">
        <div class="row">
            <!-- body header start -->
            <header class="body-header">
                <div class="container">
                    <h3>Blank page</h3>
                    <nav class="submenu">
                        <ul class="float-left">
                            <li><a href="purchase-list.html">List</a></li>
                            <li><a href="" class="active">New</a></li>
                            <li><a href="purchase-find.html">Find</a></li>
                        </ul>

                        <ul class="float-right">
                            <li><a href="#">Instruction</a></li>
                            <li><a href="#">Video</a></li>
                            <li><a href="#">Print</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
            <!-- body header end -->

            <!-- body content start -->
            <div class="container">
                <!-- content goes to here -->
                <h3>Hello World!</h3>
            </div>
            <!-- body content end -->
        </div>
    </div>
</div>
<!-- body container end -->
@endsection

