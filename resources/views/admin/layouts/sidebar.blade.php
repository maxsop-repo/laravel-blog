<!-- aside content start -->
<aside class="column aside">
    <div class="brand">
        <span>Admin Panel</span>
        <a href="#">
            <i class="ion-ios-close-empty io-24" id="aside-close"></i>
        </a>
    </div>

    <!-- aside nav start -->
    <nav class="aside-nav">
        <h6>Navigation</h6>
        <ul>
            <!-- dashboard -->
            <li id="dashboard">
                <a href="#">
                    <i class="ion-ios-timer-outline io-18"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- email -->
            <li id="email">
                <a href="#">
                    <i class="ion-ios-email-outline io-17"></i>
                    <span>Email</span>
                    <span class="badge float-right">9</span>
                </a>
            </li>
        </ul>

        <h6>Components</h6>
        <ul>
            <!-- blog -->
            <li id="menu" class="dropdown">
                <a href="#" data-target="blog-option">
                    <i class="ion-ios-book-outline io-15"></i>
                    <span>Blog</span>
                    <span class="float-right">
                        <i class="ion-ios-arrow-right right io-16"></i>
                        <i class="ion-ios-arrow-down down io-16"></i>
                    </span>
                </a>

                <ul id="blog-option">
                    <li id="list"><a href="purchase-list.html">Post</a></li>
                    <li id="add-new"><a href="purchase-add.html">Category</a></li>
                    <li id="find"><a href="purchase-find.html">Tag</a></li>
                </ul>
            </li>

            <li id="post"><a href="{{ route('post.index') }}"><i class="ion-compose io-16"></i><span>Post</span></a></li>
            <li id="post"><a href="{{ route('category.index') }}"><i class="ion-ios-folder-outline io-16"></i><span>Category</span></a></li>
            <li id="post"><a href="{{ route('tag.index') }}"><i class="ion-ios-pricetags-outline io-16"></i><span>Tag</span></a></li>

            <li id="post"><a href="{{ route('admin.media') }}" target="_blank"><i class="ion-ios-people-outline io-18"></i><span>Media</span></a></li>
            <li id="post"><a href="{{ route('user.index') }}"><i class="ion-ios-people-outline io-18"></i><span>Users</span></a></li>
            

            <!-- form -->
            {{--<li id="form">--}}
                {{--<a href="#">--}}
                    {{--<i class="ion-compose io-16"></i>--}}
                    {{--<span>Form</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <!-- purchase -->
            {{--<li id="purchase" class="dropdown">--}}
                {{--<a href="#" data-target="purchase-menu">--}}
                    {{--<i class="ion-ios-cart io-17"></i>--}}
                    {{--<span>Purchase</span>--}}
                    {{--<span class="float-right">--}}
                                    {{--<i class="ion-ios-arrow-right right io-16"></i>--}}
                                    {{--<i class="ion-ios-arrow-down down io-16"></i>--}}
                                {{--</span>--}}
                {{--</a>--}}

                {{--<ul id="purchase-menu">--}}
                    {{--<li id="list"><a href="purchase-list.html">List</a></li>--}}
                    {{--<li id="add-new"><a href="purchase-add.html">Add new</a></li>--}}
                    {{--<li id="find"><a href="purchase-find.html">Find</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <!-- table -->
            {{--<li id="table">--}}
                {{--<a href="#">--}}
                    {{--<i class="ion-ios-grid-view-outline io-16"></i>--}}
                    {{--<span>Table</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <!-- pages -->
            {{--<li id="pages" class="dropdown">--}}
                {{--<a href="#" data-target="purchase-menu">--}}
                    {{--<i class="ion-ios-bookmarks-outline io-16"></i>--}}
                    {{--<span>Pages</span>--}}
                    {{--<span class="float-right">--}}
                                    {{--<i class="ion-ios-arrow-right right io-16"></i>--}}
                                    {{--<i class="ion-ios-arrow-down down io-16"></i>--}}
                                {{--</span>--}}
                {{--</a>--}}

                {{--<ul id="purchase-menu">--}}
                    {{--<li id="list"><a href="purchase-list.html">List</a></li>--}}
                    {{--<li id="add-new"><a href="purchase-add.html">Add new</a></li>--}}
                    {{--<li id="find"><a href="purchase-find.html">Find</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <!-- layout -->
            {{--<li id="layout" class="dropdown">--}}
                {{--<a href="#" data-target="purchase-menu">--}}
                    {{--<i class="ion-card io-16"></i>--}}
                    {{--<span>Layout</span>--}}
                    {{--<span class="float-right">--}}
                                    {{--<i class="ion-ios-arrow-right right io-16"></i>--}}
                                    {{--<i class="ion-ios-arrow-down down io-16"></i>--}}
                                {{--</span>--}}
                {{--</a>--}}

                {{--<ul id="purchase-menu">--}}
                    {{--<li id="list"><a href="purchase-list.html">List</a></li>--}}
                    {{--<li id="add-new"><a href="purchase-add.html">Add new</a></li>--}}
                    {{--<li id="find"><a href="purchase-find.html">Find</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <!-- chart -->
            {{--<li id="chart">--}}
                {{--<a href="#">--}}
                    {{--<i class="ion-ios-paper-outline io-16"></i>--}}
                    {{--<span>Chaet</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>

        <h6>Your Stuff</h6>
        <ul>
            <!-- profile -->
            <li id="profile">
                <a href="#">
                    <i class="ion-android-person io-16"></i>
                    <span>Profile</span>
                    <span class="badge float-right">30%</span>
                </a>
            </li>

            <!-- document -->
            <li id="documents">
                <a href="#">
                    <i class="ion-document io-17"></i>
                    <span>Documents</span>
                    <span class="badge float-right">10</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>
<!-- aside content end -->
