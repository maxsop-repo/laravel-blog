$(document).ready(function() {

    // all the variables
    var developermode = false;
    var wrapper = $(".wrapper");
    var wrapperbg = $(".wrapper-background");

    // aside toggle, hide and show programm start
    function asideToggle() {
        if(wrapper.hasClass('aside-close')) {
            wrapper.removeClass("aside-close");
        } else {
            wrapper.addClass("aside-close");
        }
    }

    function asideHide() {
        wrapper.addClass("aside-close");
    }

    function asideShow() {
        wrapper.removeClass("aside-close");
    }

    function layer() {
        var display = false;

        // get the window width
        var winwidth = $(window).outerWidth(true);
        console.log(winwidth);

        if(winwidth <= 992) {
            if(wrapperbg.hasClass('none')) {
                wrapperbg.removeClass('none');
                display = false;
            } else {
                wrapperbg.addClass('none');
                display = true;
            }
        }

        return display;
    }

    // click on toggle button (toggle)
    $("#aside-toggle").on("click", function(event) {
        asideToggle();
        layer();

        event.preventDefault();
    });

    // click on the wrapper-background (close)
    $(".wrapper-background").on("click", function() {
        asideHide();
        layer();
    });

    // click on the aside-close (close)
    $('i#aside-close').on("click", function() {
        asideHide();
        layer();
    });

    // on resize event aside toggle (toggle, optional)
    $(window).on("resize", function() {
        if(developermode) {
            asideToggle();
            layer();
        }
    });
    // aside toggle, hide and show programm end




    // dropdown toggle programm start
    $(".dropdown > a").on("click", function(event) {
        var parent = $(this).closest("li.dropdown");

        if(parent.hasClass("active")) {
            parent.removeClass("active");
        } else {
            parent.addClass("active");
            $( "li.dropdown" ).not( parent ).removeClass( "active" );
        }

        event.preventDefault();
    });
    // dropdown toggle programm end



    // nicescroll plugin
    $(".aside-nav, .company-title, .settings-menu").niceScroll({
        cursorcolor: 'rgba(152, 166, 173, 0.5)',
        cursorwidth: '6px',
        cursorborderradius: '0px'
    });



    // list dropdown
    var menu = $('.wrapper').data('menu'),
        submenu = $('.wrapper').data('submenu');

    var li = '.aside-nav li#' + menu;

    $(li).addClass('active');
    $(li + ' ul li#' + submenu).addClass('active');

    // settings part
    $('.settings').on('click', function(event) {
        var settings = $("div.settings-content");

        if(settings.hasClass('open')) {
            settings.removeClass('open');
        } else {
            settings.addClass('open');
        }

        event.preventDefault();
    });

});
