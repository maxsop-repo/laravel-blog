/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	config.height = '300';
    // https://ckeditor.com/latest/samples/old/toolbar/toolbar.html
    config.toolbarGroups = [
    	{ name: 'undo', groups: [ 'undo' ] },
    	{ name: 'styles' },
    	{ name: 'basicstyles', groups: [ 'basicstyles' ] },
    	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
    	{ name: 'links' },
    	{ name: 'insert', groups: [ 'insert', 'hello' ] },
    	{ name: 'colors' },
    	{ name: 'document', groups: [ 'mode', 'document' ], }
    ];

    config.extraPlugins = 'hello';
    config.removeButtons = 'Styles,Font,Subscript,Superscript,CreateDiv,Flash,Smiley,SpecialChar,PageBreak,Iframe,Save,NewPage,Preview,Print,Anchor';
    config.contentsCss = '/admin/css/ckeditor_contents_styles.css';
	
};