@extends('admin.layouts.app')

@section('main-content')
    <!-- body container start -->
    <div class="body-container">
        <div class="container-fluid">
            <div class="row">
                <!-- body header start -->
                <header class="body-header">
                    <div class="container">
                        <h3>Category edit</h3>
                        <nav class="submenu">
                            <ul class="float-left">
                                <li><a href="purchase-list.html">List</a></li>
                                <li><a href="" class="active">New</a></li>
                                <li><a href="purchase-find.html">Find</a></li>
                            </ul>

                            <ul class="float-right">
                                <li><a href="#">Instruction</a></li>
                                <li><a href="#">Video</a></li>
                                <li><a href="#">Print</a></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <!-- body header end -->

                <!-- body content start -->
                <div class="container">
                    <!-- content goes to here -->
                    <h3>Create Category</h3>
                    <!-- Create the editor container -->
                    <small>Get formula help from "https://katex.org/docs/supported.html"</small>

                    @include('partial/messages')

                    <form action="{{ route('category.update', $category->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="title">Category Title</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ $category->name }}" required>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="slug">Category Slug</label>
                                <input type="text" name="slug" id="slug" class="form-control" value="{{ $category->slug }}" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <button type="submit" class="btn submit-btn float-right">Update</button>
                                <a href="{{ route('category.index') }}" class="btn reset-btn float-right">Back</a>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>
    <!-- body container end -->
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush
