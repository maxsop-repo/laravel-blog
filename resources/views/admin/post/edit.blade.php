@extends('admin.layouts.app')

@section('main-content')
    <!-- body container start -->
    <div class="body-container">
        <div class="container-fluid">
            <div class="row">
                <!-- body header start -->
                <header class="body-header">
                    <div class="container">
                        <h3>Post edit</h3>
                        <nav class="submenu">
                            <ul class="float-left">
                                <li><a href="purchase-list.html">List</a></li>
                                <li><a href="" class="active">New</a></li>
                                <li><a href="purchase-find.html">Find</a></li>
                            </ul>

                            <ul class="float-right">
                                <li><a href="#">Instruction</a></li>
                                <li><a href="#">Video</a></li>
                                <li><a href="#">Print</a></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <!-- body header end -->

                <!-- body content start -->
                <div class="container">
                    <!-- content goes to here -->
                    <h3>Create Post</h3>
                    <!-- Create the editor container -->
                    <small>For math equation: "<strong>https://ckeditor.com/cke4/addon/mathjax</strong>"</small>
                    
                    @include('partial/messages')

                    <form action="{{ route('post.update', $post->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
                            </div>

                            <div class="form-group col-md-6 col-sm-6 required">
                                <label for="subtitle">Subtitle</label>
                                <input type="text" name="subtitle" id="subtitle" class="form-control" value="{{ $post->subtitle }}">
                            </div>

                            <div class="form-group col-md-6 col-sm-6 required">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" id="slug" class="form-control" value="{{ $post->slug }}">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="editor">Content</label>
                                <textarea name="content" id="editor">{{ $post->body }}</textarea>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="file">File Import</label>
                                <input type="file" name="upload" id="file" class="form-control">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <div class="form-check float-left">
                                    <input class="form-check-input" type="checkbox" name="publish" id="publish" @if ($post->status == 1 ) checked @endif>
                                    <label for="publish">Publish</label>
                                </div>

                                <button type="submit" name="store" class="btn submit-btn float-right">Update</button>
                                <a href="{{ route('post.index') }}" class="btn reset-btn float-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>
    <!-- body container end -->
@endsection

@push('styles')
    <style type="text/css">
        .cke_screen_reader_only.cke_copyformatting_notification {display: none;}
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('admin/vendors/ckeditor/ckeditor.js') }}"></script>
    
    <script type="text/javascript">
        window.onload = function() {
            // https://ckeditor.com/docs/ckeditor4/latest/guide/dev_installation.html
            var editor = CKEDITOR.replace( 'editor', {
                customConfig: '/admin/js/ckeditor/ckeditor_config.js',
            } );

            // editor event
            editor.on('change', function(event) {
                console.log( 'Total bytes: ' + event.editor.getData().length );
                // console.log( event.editor.getData() );
            });
        };
    </script>
@endpush