<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User route
Route::group(['namespace' => 'User'], function() {
	Route::get('/', 'HomeControlle@index');
	Route::get('post', 'PostControlle@index')->name('post');
});

// Admin route
Route::group(['namespace' => 'Admin'], function() {
	Route::get('admin/home', 'HomeControlle@index')->name('admin.name');
	// User route
	Route::resource('admin/user', 'UserControlle');
	// Category route
	Route::resource('admin/category', 'CategoryController');
	// Post route
	Route::resource('admin/post', 'PostController');
	// Tag route
	Route::resource('admin/tag', 'TagController');
	// Media upload route
	Route::get('admin/media', 'MediaController@index')->name('admin.media');
});



