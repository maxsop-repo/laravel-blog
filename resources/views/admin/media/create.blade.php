@extends('admin.layouts.app')

@section('main-content')
    <!-- body container start -->
    <div class="body-container">
        <div class="container-fluid">
            <div class="row">
                <!-- body header start -->
                <header class="body-header">
                    <div class="container">
                        <h3>Post</h3>
                        <nav class="submenu">
                            <ul class="float-left">
                                <li><a href="purchase-list.html">List</a></li>
                                <li><a href="" class="active">New</a></li>
                                <li><a href="purchase-find.html">Find</a></li>
                            </ul>

                            <ul class="float-right">
                                <li><a href="#">Instruction</a></li>
                                <li><a href="#">Video</a></li>
                                <li><a href="#">Print</a></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <!-- body header end -->

                <!-- body content start -->
                <div class="container">
                    <!-- content goes to here -->
                    <h3>Create Post</h3>
                    <!-- Create the editor container -->
                    <small>Get formula help from "https://katex.org/docs/supported.html"</small>
                    
                    <div>
                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>


                    <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="subtitle">Subtitle</label>
                                <input type="text" name="subtitle" id="subtitle" class="form-control">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" id="slug" class="form-control">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="file">File Import</label>
                                <input type="file" name="upload" id="file" class="form-control">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 required">
                                <label for="editor">Content</label>
                                <textarea name="content" id="editor"></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12">
                                <div class="form-check float-left">
                                    <input class="form-check-input" type="checkbox" name="publish" id="publish">
                                    <label for="publish">Publish</label>
                                </div>

                                <button type="submit" name="store" class="btn submit-btn float-right">Store</button>
                                <button type="reset" name="reset" class="btn reset-btn float-right">Reset</button>
                            </div>
                        </div>
                    </form>
                    
                    <div>
                        <p>For math equation: https://ckeditor.com/cke4/addon/mathjax</p>
                    </div>
                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>
    <!-- body container end -->
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush
