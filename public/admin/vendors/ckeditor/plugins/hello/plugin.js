CKEDITOR.plugins.add( 'hello', {
    icons: 'hello',
    init: function( editor ) {
        // Plugin logic goes here.
        editor.ui.addButton( 'Hello', {
            label: 'Insert Hello',
            command: 'insertHello',
            toolbar: 'insert'
        });

        editor.addCommand( 'insertHello', {
            exec: function( editor ) {
                editor.insertHtml( '<h1>Hello World!</h1><table><tr><td>Name</td><td>Maruf Hasan</td></tr></table>' );
				console.log(editor.getData());
            }
        });
    }
});