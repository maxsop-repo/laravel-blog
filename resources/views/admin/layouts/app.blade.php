<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.layouts.head')
    </head>

    <body>
        <section class="wrapper">
            @include('admin.layouts.sidebar')

            @include('admin.layouts.settings')

            <!-- body section start -->
            <div class="column body">
                @include('admin.layouts.header')

                @section('main-content')
                    @show
            </div>
            <!-- body section end -->
        </section>

        @include('admin.layouts.footer')
    </body>
</html>
