@extends('admin.layouts.app')

@section('main-content')
    <!-- body container start -->
    <div class="body-container">
        <div class="container-fluid">
            <div class="row">
                <!-- body header start -->
                <header class="body-header">
                    <div class="container">
                        <h3>Category </h3>
                        <nav class="submenu">
                            <ul class="float-left">
                                <li><a href="purchase-list.html">List</a></li>
                                <li><a href="" class="active">New</a></li>
                                <li><a href="purchase-find.html">Find</a></li>
                            </ul>

                            <ul class="float-right">
                                <li><a href="#">Instruction</a></li>
                                <li><a href="#">Video</a></li>
                                <li><a href="#">Print</a></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <!-- body header end -->

                <!-- body content start -->
                <div class="container">
                    <!-- list table -->
                    <div>
                        <h3>All category</h3>
                        <a href="{{ route('category.create') }}">Add new</a>
                    </div>

                    <div class="table-responsive-sm">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th class="table-action">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <th>{{ $loop->index + 1 }}</th>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td class="table-action">
                                            <a href="{{ route('category.edit', $category->id) }}"><i class="ion-edit io-14"></i></a>

                                            <form action="{{ route('category.destroy', $category->id) }}" method="post" id="delete-form-{{ $category->id }}" style="display: none;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>

                                            <a href="{{ route('category.index') }}" onClick="if(confirm('Are you sure, You want to delete this?')) {event.preventDefault();document.getElementById('delete-form-{{ $category->id }}').submit();} else {event.preventDefault();}"><i class="ion-android-delete io-18"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <th>SL No</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th class="table-action">Action</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- pagination -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-end">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>
                <!-- body content end -->
            </div>
        </div>
    </div>
    <!-- body container end -->
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush
