<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Blank page</title>
<!-- bootstrap -->
<link rel="stylesheet" href="{{ asset('admin/vendors/bootstrap/css/bootstrap.min.css') }}">
<!-- icons -->
<link rel="stylesheet" href="{{ asset('admin/vendors/ionicons/css/ionicons.min.css') }}">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600" rel="stylesheet">
<!-- include style -->
<link rel="stylesheet" href="{{ asset('admin/css/master.css') }}">

@stack('styles')


